# `brine` 🌊
A currently hypothetical WebAssembly to Verilog compiler. (The name `brine` refers to the "sea of gates" on an FPGA.)

## Overview
[WebAssembly](https://en.wikipedia.org/wiki/WebAssembly) is a new standard that defines a simple, portable assembly format. Many languages can already compile to WebAssembly, 

[Verilog](https://en.wikipedia.org/wiki/Verilog) is a widely-supported hardware description language that can be compiled to run on ASICs and FPGAs. (...)

The goal of this project is to make it easy for software developers to easily develop *hardware* from *any* supported programming language. Any programming language can be supported 
This should allow software developers to take advantage of 

## How it works
Install brine (and its cargo wrapper):
```sh
$ cargo install cargo-brine
```

Create a new rust project:
`Cargo.toml`:
```
[package]
authors = ["James Gilles <jhgilles@mit.edu>"]
name = "brine-example"
version = "0.1.0"

[dependencies]
brine = "0.1.0"
```

`lib.rs`:
```rust
use brine::{entry};

#[entry]
fn gcd(a: u32, b: u32) -> u32 {
    if b == 0 {
        a
    } else {
        gcd(b, a % b)
    }
}
```

Run:
```sh
$ cargo brine verilog
```

This transparently compiles your rust code to WebAssembly, then compiles that WebAssembly to Verilog.

Output:
```verilog
// yadda yadda
```

Run:
```sh
$ cargo brine icestick
```
To deploy your code to a low-cost ICEStick USB FPGA.

## Features
(Note that all of these features are currently hypothetical.)
- Mixed behavioral and structural synthesis
- Many-language support
- Importing of external verilog IPs
- Easy circuit simulation in WebAssembly
- Easy circuit simulation via Verilator
- Mixed clock domains

### Language support
The following languages have actively developed support:
- Rust

The following languages are not actively developed but would be easy to port:
- C/C++
- Go
- JavaScript, via [AssemblyScript](https://github.com/AssemblyScript/assemblyscript)
- Haskell, via [Asterius](https://github.com/tweag/asterius) 
- Java/Kotlin/Scala, via [TeaVM](http://www.teavm.org/)

Any language that can compile directly to good-quality webassembly should produce good results with Brine.

The following languages are unlikely to be ported:
- Python
- Ruby
- Lua
- PHP

Brine doesn't understand languages that must be interpreted when running in WebAssembly.

### Annotations
There are some properties we'd like to have in our generated code that can't be expressed in WebAssembly: for instance, custom-bitwidth integers, fixed point numbers, tristate gates, and so on.

These can be expressed by importing the "brine.annotations" library and sprinkling annotation function calls through your code. For instance, `brine.annotations.uintwidth(input: u64, width: u32)` marks an input value as having a particular width.

## FAQ
- why?
    - lmao

## Alternatives
This project is currently not functional. Some useful open-source alternatives include:
- [LegUp](http://legup.eecg.utoronto.ca/), a high-performance behavioral synthesis tool for C/C++
- [Chisel](https://chisel.eecs.berkeley.edu/) and [Spinal](https://github.com/SpinalHDL/SpinalHDL),
  easy-to-use structural synthesis tools for JVM languages
