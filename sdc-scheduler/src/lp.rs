use super::cdfg::{Block, Graph, Value, ValueData, BranchData};
use lp_modeler::{
    // see cong for proof that continuous problem has integer solutions
    variables::LpContinuous as LpVar,
    problem::{LpProblem, LpObjective}
};
use std::collections::HashMap;

#[derive(Clone)]
pub struct Schedule {
    stages: Vec<Stage>
}

#[derive(Clone)]
pub struct Stage {
    /// data nodes to evaluate in this stage
    values: Vec<Value>,
    /// control flow nodes to evaluate in this stage
    finish_blocks: Vec<Block>
}

/// The scheduling objective for this graph.
#[derive(Clone, PartialEq, Eq)]
pub enum Objective {
    ASAP
}

pub fn schedule(graph: Graph, objective: Objective) -> Schedule {
    // the scheduling algorithm is relatively simple.
    // each node in the graph is mapped to a variable in a linear optimization problem.
    // if node 5's variable is mapped to the number 13, that means that node 5 is executed in step 13.

    // due to Linear Algebra (??), we can use a continuous LP solver and be guaranteed integer solutions.
    let mut dvars = HashMap::<Value, LpVar>::new();

    // blocks have super-sources and super-sinks... for reasons
    let mut sources = HashMap::<Block, LpVar>::new();
    let mut sinks = HashMap::<Block, LpVar>::new();

    for value in graph.values.keys() {
        dvars.insert(value, LpVar::new(&format!("{:?}", value)));
    }
    for block in graph.blocks.keys() {
        sources.insert(block, LpVar::new(&format!("src{:?}", block)));
        sinks.insert(block, LpVar::new(&format!("sink{:?}", block)));
    }

    let mut problem = match objective {
        Objective::ASAP => {
            // schedule all keys as soon as possible:
            // minimize scheduling variables
            // TODO: custom names?
            let mut problem = LpProblem::new("sdclp0", LpObjective::Minimize);
            for value in graph.values.keys() {
                problem += &dvars[&value];
            }
            problem
        }
    };

    // data dependency constraints
    for (value, data) in &graph.values {
        data.with_deps(|dep| {
            // add dep from value -> dep

        })
    }

    for (block, data) in &graph.blocks {
        if let Some(branch) = data.branch {
            branch.with_targets(|target| {
                // add dep from block target -> block

            })
        }
    }

    // call solver

    // restructure solver results

    unimplemented!()
}
