/// A simple untyped control-dataflow graph; an unscheduled digraph of operations to perform.
/// Not optimized.

use std::collections::{HashMap, HashSet};
use std::iter::IntoIterator;
use cranelift_entity::{PrimaryMap, entity_impl};

#[derive(Clone, Copy, Debug)]
pub enum BranchData {
    Brz { value: Value, zero: Block, nonzero: Block },
    Goto { block: Block }
}
impl BranchData {
    pub fn with_targets(&self, mut op: impl FnMut(Block)) {
        use self::BranchData::*;
        match self {
            Brz { zero, nonzero, .. } => { op(*zero); op(*nonzero) },
            Goto { block } => op(*block)
        }
    }
}

#[derive(Clone, Debug)]
pub struct BlockData {
    /// list of all data nodes in the block
    pub(crate) values: Vec<Value>,
    /// comment on the block
    pub(crate) annotation: String,
    /// where to branch to
    pub(crate) branch: Option<BranchData>
}

/// A basic block.
/// Contains an acyclic digraph of data operations.
#[derive(Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub struct Block(pub u32);
entity_impl!(Block, "bb");

#[derive(Clone, Debug)]
pub enum ValueData {
    Input { name: String },
    Output { a: Value, name: String },
    Raw { value: u32 },
    Phi { choices: HashMap<Block, Value> },
    Add { a: Value, b: Value },
    Sub { a: Value, b: Value },
    Mul { a: Value, b: Value },
    Div { a: Value, b: Value },
    Equal { a: Value, b: Value },
    LessThan { a: Value, b: Value },
    And { a: Value, b: Value },
    Not { a: Value },
}
impl ValueData {
    pub fn with_deps(&self, mut op: impl FnMut(Value)) {
        use self::ValueData::*;

        match self {
            Output { a, .. } =>  op(*a),
            Not { a } =>         op(*a),
            Add { a, b } =>      { op(*a); op(*b) },
            Sub { a, b } =>      { op(*a); op(*b) },
            Mul { a, b } =>      { op(*a); op(*b) },
            Div { a, b } =>      { op(*a); op(*b) },
            Equal { a, b } =>    { op(*a); op(*b) },
            LessThan { a, b } => { op(*a); op(*b) },
            And { a, b } =>      { op(*a); op(*b) },
            Phi { ref choices } => {
                for choice in choices.values() {
                    op(*choice)
                }
            },
            Input { .. } => (),
            Raw { .. } => (),
        }
    }
}

/// An SSA value.
#[derive(Clone, Copy, PartialOrd, Ord, PartialEq, Eq, Hash)]
pub struct Value(pub u32);
entity_impl!(Value, "v");

#[derive(Clone, Debug)]
pub struct Graph {
    pub(crate) blocks: PrimaryMap<Block, BlockData>,
    pub(crate) values: PrimaryMap<Value, ValueData>,
    pub(crate) inputs: HashSet<String>,
    pub(crate) outputs: HashSet<String>,

    /// in order
    pub(crate) order: Vec<Block>,
}
impl Graph {
    pub fn new() -> Self {
        Graph {
            blocks: PrimaryMap::new(),
            values: PrimaryMap::new(),
            inputs: HashSet::new(),
            outputs: HashSet::new(),
            order: Vec::new(),
        }
    }

    pub fn block(&mut self, annotation: impl Into<String>) -> Block {
        let block = self.blocks.push(BlockData {
            values: vec![],
            annotation: annotation.into(),
            branch: None
        });
        self.order.push(block);
        block
    }

    pub fn edit(&mut self, block: Block, edit: impl FnOnce(&mut BlockBuilder)) {
        edit(&mut self.builder(block))
    }

    pub fn builder(&mut self, block: Block) -> BlockBuilder {
        BlockBuilder {
            graph: self,
            block
        }
    }

    pub fn interpret(&self, inputs: &[(&str, u32)]) -> HashMap<String, u32> {
        let inputs: HashMap<String, u32> = inputs.into_iter().map(|&(k, v)| (k.into(), v)).collect();
        self.interpret_hashmap(inputs)
    }

    #[inline(never)]
    pub fn interpret_hashmap(&self, inputs: HashMap<String, u32>) -> HashMap<String, u32> {
        let mut current = self.order[0];
        let mut outputs = HashMap::new();
        let mut values: HashMap<Value, u32> = HashMap::new();
        let mut previous = None;
        'interp: loop {
            for vid in &self.blocks[current].values {
                assert!(!values.contains_key(&vid), "can't reassign to ssa variable {}", vid);
                let result = match &self.values[*vid] {
                    &ValueData::Raw { value }          => value,
                    &ValueData::Phi { ref choices }    => values[&choices[&previous.unwrap()]],
                    &ValueData::Add { a, b }           => values[&a] + values[&b],
                    &ValueData::Sub { a, b }           => values[&a] - values[&b],
                    &ValueData::Mul { a, b }           => values[&a] * values[&b],
                    &ValueData::Div { a, b }           => values[&a] / values[&b],
                    &ValueData::Equal { a, b }         => (values[&a] == values[&b]) as u32,
                    &ValueData::LessThan { a, b }      => (values[&a] == values[&b]) as u32,
                    &ValueData::And { a, b }           => values[&a] & values[&b],
                    &ValueData::Not { a }              => values[&a],
                    &ValueData::Input { ref name }     => inputs[name],
                    &ValueData::Output { ref name, a } => {
                        assert!(!outputs.contains_key(name), "can't reassign to output {}", name);
                        outputs.insert(name.clone(), values[&a]);
                        values[&a]
                    },
                };
                values.insert(*vid, result);
            }
            previous = Some(current);
            current = match self.blocks[current].branch {
                Some(BranchData::Brz { value, zero, nonzero }) => {
                    if values[&value] == 0 {
                        zero
                    } else {
                        nonzero
                    }
                },
                Some(BranchData::Goto { block }) => block,
                None => break 'interp
            }
        }
        outputs
    }
}

pub struct BlockBuilder<'graph> {
    graph: &'graph mut Graph,
    block: Block
}
impl<'graph> BlockBuilder<'graph> {
    fn add_op(&mut self, data: ValueData) -> Value {
        let op = self.graph.values.push(data);
        self.graph.blocks[self.block].values.push(op);
        op
    }
    pub fn add(&mut self, a: Value, b: Value) -> Value {
        self.add_op(ValueData::Add { a, b })
    }
    pub fn sub(&mut self, a: Value, b: Value) -> Value {
        self.add_op(ValueData::Sub { a, b })
    }
    pub fn mul(&mut self, a: Value, b: Value) -> Value {
        self.add_op(ValueData::Mul { a, b })
    }
    pub fn div(&mut self, a: Value, b: Value) -> Value {
        self.add_op(ValueData::Div { a, b })
    }
    pub fn equal(&mut self, a: Value, b: Value) -> Value {
        self.add_op(ValueData::Equal { a, b })
    }
    pub fn lessthan(&mut self, a: Value, b: Value) -> Value {
        self.add_op(ValueData::LessThan { a, b })
    }
    pub fn and(&mut self, a: Value, b: Value) -> Value {
        self.add_op(ValueData::And { a, b })
    }
    pub fn raw(&mut self, value: u32) -> Value {
        self.add_op(ValueData::Raw { value })
    }
    pub fn not(&mut self, a: Value) -> Value {
        self.add_op(ValueData::Not { a })
    }
    pub fn phi<'a, T: IntoIterator<Item=&'a (Block, Value)>>(&mut self, choices: T) -> Value {
        self.add_op(ValueData::Phi { choices: choices.into_iter().cloned().collect() })
    }
    pub fn input(&mut self, name: impl Into<String>) -> Value {
        let name = name.into();
        self.graph.inputs.insert(name.clone());
        self.add_op(ValueData::Input { name })
    }
    pub fn output(&mut self, name: impl Into<String>, a: Value) -> Value {
        let name = name.into();
        self.graph.outputs.insert(name.clone());
        self.add_op(ValueData::Output { a, name })
    }

    pub fn brz(&mut self, value: Value, zero: Block, nonzero: Block) {
        self.graph.blocks[self.block].branch = Some(BranchData::Brz { value, zero, nonzero });
    }
    pub fn goto(&mut self, block: Block) {
        self.graph.blocks[self.block].branch = Some(BranchData::Goto { block });
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic() {
        let mut graph = Graph::new();
        let b1 = graph.block("start block");
        graph.edit(b1, |b| {
            let v1 = b.input("input_a");
            let v2 = b.input("input_b");
            let v3 = b.add(v1, v2);
            b.output("output_a", v3);
        });
        let outputs = graph.interpret(&[
            ("input_a", 1),
            ("input_b", 2),
        ]);
        assert_eq!(outputs["output_a"], 3);
    }

    #[test]
    fn branch() {
        let mut graph = Graph::new();
        let b1 = graph.block("start block");
        let bz = graph.block("zero");
        let bnz = graph.block("nonzero");
        
        graph.edit(b1, |b| {
            let v1 = b.input("input_a");
            b.brz(v1, bz, bnz);
        });
        graph.edit(bz, |b| {
            let v2 = b.raw(0);
            b.output("output_a", v2);
        });
        graph.edit(bnz, |b| {
            let v2 = b.raw(1);
            b.output("output_a", v2);
        });

        let outputsz = graph.interpret(&[
            ("input_a", 0)
        ]);
        assert_eq!(outputsz["output_a"], 0);

        let outputsnz = graph.interpret(&[
            ("input_a", 1)
        ]);
        assert_eq!(outputsnz["output_a"], 1);

    }

    #[test]
    fn phi() {
        let mut graph = Graph::new();
        let b1 = graph.block("start block");
        let bz = graph.block("zero");
        let bnz = graph.block("nonzero");
        let bfinal = graph.block("final");
        
        graph.edit(b1, |b| {
            let v1 = b.input("input_a");
            b.brz(v1, bz, bnz);
        });
        graph.edit(bz, |b| {
            b.goto(bfinal);
        });
        graph.edit(bnz, |b| {
            b.goto(bfinal);
        });
        graph.edit(bfinal, |b| {
            let z = b.raw(0);
            let nz = b.raw(1);
            let v = b.phi(&[(bz, z), (bnz, nz)]);
            b.output("output_a", v);
        });

        let outputsz = graph.interpret(&[
            ("input_a", 0)
        ]);
        assert_eq!(outputsz["output_a"], 0);

        let outputsnz = graph.interpret(&[
            ("input_a", 1)
        ]);
        assert_eq!(outputsnz["output_a"], 1);


    }
}
