# sdc-scheduler
This crate implements a System of Difference Constraints-based scheduler for control flow graphs, as described in [Cong '06](http://www.csl.cornell.edu/~zhiruz/pdfs/sdcmod-iccad2013.pdf).

Given a control flow graph, a set of scheduling constraints, and an optimization goal, it will schedule the graph into a sequence of "control states" - sets of operations that can be executed in parallel.

## Algorithm description
CDFG: "control data flow graph"

## Implementation notes
We steal several implementation details from `CraneLift`; in particular, `cranelift_entity`, which is like a specialized version of `slotmap` for building up SSA-based representations of code.